# Swedish translation of release-notes for Debian
# Copyright (C) 2006, 2007, 2008, 2009, 2011, 2013, 2017 Free Software Foundation, Inc.
#
# Martin Bagge <brother@bsnet.se>, 2009, 2011, 2013, 2017.
# Martin Ågren <martin.agren@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: release-notes moreinfo.po\n"
"POT-Creation-Date: 2017-06-12 09:36+0200\n"
"PO-Revision-Date: 2017-06-13 22:19+0200\n"
"Last-Translator: Martin Bagge / brother <brother@bsnet.se>\n"
"Language-Team: Swedish <debian-l10n-swedish@lists.debian.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.1\n"

#. type: Attribute 'lang' of: <chapter>
#: en/moreinfo.dbk:8
msgid "en"
msgstr "sv"

#. type: Content of: <chapter><title>
#: en/moreinfo.dbk:9
msgid "More information on &debian;"
msgstr "Mer information om &debian;"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:11
msgid "Further reading"
msgstr "Ytterligare läsning"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:13
msgid ""
"Beyond these release notes and the installation guide, further documentation "
"on Debian is available from the Debian Documentation Project (DDP), whose "
"goal is to create high-quality documentation for Debian users and "
"developers, such as the Debian Reference, Debian New Maintainers Guide, the "
"Debian FAQ, and many more.  For full details of the existing resources see "
"the <ulink url=\"&url-ddp;\">Debian Documentation website</ulink> and the "
"<ulink url=\"&url-wiki;\">Debian Wiki</ulink>."
msgstr ""
"Förutom dessa kommentarer till utgåvan och installationsguiden finns "
"ytterligare dokumentation om Debian tillgänglig från Debian Documentation "
"Project (DDP), som har som mål att skapa högkvalitativ dokumentation för "
"Debiananvändare och -utvecklare. Dokumentation, som till exempel Debian "
"Reference, Debian New Maintainers Guide, Debian FAQ och flera andra. För "
"fullständiga detaljer om tillgängliga resurser se webbplatsen för <ulink url="
"\"&url-ddp;\">Debian Documentation Project</ulink> och <ulink url=\"&url-"
"wiki;\">Debians Wiki</ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:23
msgid ""
"Documentation for individual packages is installed into <filename>/usr/share/"
"doc/<replaceable>package</replaceable></filename>.  This may include "
"copyright information, Debian specific details, and any upstream "
"documentation."
msgstr ""
"Dokumentation för individuella paket installeras i <filename>/usr/share/doc/"
"<replaceable>paket</replaceable></filename>. Den kan inkludera information "
"om upphovsrätt, Debianspecifika detaljer och dokumentation från utgivaren."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:31
msgid "Getting help"
msgstr "Få hjälp"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:33
msgid ""
"There are many sources of help, advice, and support for Debian users, though "
"these should only be considered after researching the issue in available "
"documentation.  This section provides a short introduction to these sources "
"which may be helpful for new Debian users."
msgstr ""
"Det finns många källor för hjälp, råd och stöd för Debiananvändare, men "
"dessa bör endast användas om dokumentationen inte har hjälpt till att lösa "
"problemet. Det här kapitlet tillhandahåller en kort introduktion till dessa, "
"vilket kan vara till hjälp för nya Debiananvändare."

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:39
msgid "Mailing lists"
msgstr "Sändlistor"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:41
msgid ""
"The mailing lists of most interest to Debian users are the debian-user list "
"(English) and other debian-user-<replaceable>language</replaceable> lists "
"(for other languages).  For information on these lists and details of how to "
"subscribe see <ulink url=\"&url-debian-list-archives;\"></ulink>.  Please "
"check the archives for answers to your question prior to posting and also "
"adhere to standard list etiquette."
msgstr ""
"De sändlistor som är mest intressanta för Debian-användarna är listan debian-"
"user (engelsk) och andra debian-user-<replaceable>språk</replaceable>-listor "
"(för andra språk; <literal>swedish</literal> för svenska). För information "
"om dessa listor och detaljer om hur man prenumererar, se <ulink url=\"&url-"
"debian-list-archives;\"></ulink>. Leta i arkiven efter svar på dina frågor "
"innan du postar en ny fråga, så bryter du inte mot listetiketten."

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:51
msgid "Internet Relay Chat"
msgstr "Internet Relay Chat"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:53
msgid ""
"Debian has an IRC channel dedicated to support and aid for Debian users, "
"located on the OFTC IRC network.  To access the channel, point your favorite "
"IRC client at irc.debian.org and join <literal>#debian</literal>."
msgstr ""
"Debian har en IRC-kanal vars ändamål är att ge stöd och hjälp till "
"Debiananvändare. Kanalen finns på IRC-nätverket OFTC. För att komma åt "
"kanalen, peka din favorit-IRC-klient till irc.debian.org och gå in i kanalen "
"<literal>#debian</literal>."

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:58
msgid ""
"Please follow the channel guidelines, respecting other users fully.  The "
"guidelines are available at the <ulink url=\"&url-wiki;DebianIRC\">Debian "
"Wiki</ulink>."
msgstr ""
"Följ kanalens riktlinjer och respektera andra användare. Riktlinjerna finns "
"tillgängliga på <ulink url=\"&url-wiki;DebianIRC\">Debians wiki</ulink>."

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:63
msgid ""
"For more information on OFTC please visit the <ulink url=\"&url-irc-host;"
"\">website</ulink>."
msgstr ""
"För mer information om OFTC, besök dess <ulink url=\"&url-irc-host;"
"\">webbplats</ulink>."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:71
msgid "Reporting bugs"
msgstr "Rapportera fel"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:73
msgid ""
"We strive to make Debian a high-quality operating system; however that does "
"not mean that the packages we provide are totally free of bugs.  Consistent "
"with Debian's <quote>open development</quote> philosophy and as a service to "
"our users, we provide all the information on reported bugs at our own Bug "
"Tracking System (BTS).  The BTS can be browsed at <ulink url=\"&url-bts;\"></"
"ulink>."
msgstr ""
"Vi strävar mot att göra Debian till ett högkvalitativt operativsystem. Det "
"betyder dock inte att paketen som vi tillhandahåller är helt felfria. I "
"enlighet med Debians filosofi om <quote>öppen utveckling</quote> och som en "
"tjänst till våra användare, tillhandahåller vi all information om "
"rapporterade fel i vårt eget felhanteringssystem (BTS). BTS är bläddringsbar "
"på <ulink url=\"&url-bts;\"></ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:81
msgid ""
"If you find a bug in the distribution or in packaged software that is part "
"of it, please report it so that it can be properly fixed for future "
"releases.  Reporting bugs requires a valid e-mail address.  We ask for this "
"so that we can trace bugs and developers can get in contact with submitters "
"should additional information be needed."
msgstr ""
"Om du hittar ett fel i distributionen eller i paketerad programvara som är "
"en del av den, vänligen rapportera felet så att det kan rättas till i "
"framtida utgåvor. Felrapportering kräver att du har en giltig e-postadress. "
"Vi frågar efter den så att vi kan spåra fel och för att utvecklarna ska "
"kunna komma i kontakt med de som rapporterat felet ifall de skulle behöva "
"ytterligare information."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:88
msgid ""
"You can submit a bug report using the program <command>reportbug</command> "
"or manually using e-mail.  You can find out more about the Bug Tracking "
"System and how to use it by reading the reference documentation (available "
"at <filename>/usr/share/doc/debian</filename> if you have <systemitem role="
"\"package\">doc-debian</systemitem> installed) or online at the <ulink url="
"\"&url-bts;\">Bug Tracking System</ulink>."
msgstr ""
"Du kan skicka in en felrapport med programmet <command>reportbug</command> "
"eller manuellt via e-post. Du kan läsa mer om felhanteringssystemet och hur "
"det används genom att läsa referensdokumentationen (tillgänglig i <filename>/"
"usr/share/doc/debian</filename> om du har paketet <systemitem role=\"package"
"\">doc-debian</systemitem> installerat) eller i <ulink url=\"&url-bts;"
"\">felhanteringssystemet</ulink>."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:98
msgid "Contributing to Debian"
msgstr "Att bidra till Debian"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:100
msgid ""
"You do not need to be an expert to contribute to Debian.  By assisting users "
"with problems on the various user support <ulink url=\"&url-debian-list-"
"archives;\">lists</ulink> you are contributing to the community.  "
"Identifying (and also solving) problems related to the development of the "
"distribution by participating on the development <ulink url=\"&url-debian-"
"list-archives;\">lists</ulink> is also extremely helpful.  To maintain "
"Debian's high-quality distribution, <ulink url=\"&url-bts;\">submit bugs</"
"ulink> and help developers track them down and fix them.  The tool "
"<systemitem role=\"package\">how-can-i-help</systemitem> helps you to find "
"suitable reported bugs to work on.  If you have a way with words then you "
"may want to contribute more actively by helping to write <ulink url=\"&url-"
"ddp-svn-info;\">documentation</ulink> or <ulink url=\"&url-debian-i18n;"
"\">translate</ulink> existing documentation into your own language."
msgstr ""
"Du behöver inte vara en expert för att bidra till Debian. Genom att hjälpa "
"användare med problem på de olika <ulink url=\"&url-debian-list-archives;"
"\">sändlistorna</ulink> för användarstöd bidrar du till gemenskapen. "
"Identifiering (och lösning) av problem relaterade till utveckling av "
"distributionen genom att delta i <ulink url=\"&url-debian-list-archives;"
"\">sändlistorna</ulink> för utveckling är också mycket uppskattat. För att "
"underhålla Debians högkvalitativa distribution kan du <ulink url=\"&url-bts;"
"\">skicka in felrapporter</ulink> och hjälpa utvecklarna att spåra upp och "
"rätta till felen. Verktyget <systemitem role=\"package\">how-can-i-help</"
"systemitem> pekar ut felrapporter som du kan arbeta på. Om du är bra på att "
"sätta ihop ord kanske du vill bidra mer aktivt genom att hjälpa till att "
"skriva <ulink url=\"&url-ddp-svn-info;\">dokumentation</ulink> eller <ulink "
"url=\"&url-debian-i18n;\">översätta</ulink> befintlig dokumentation till "
"ditt eget språk."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:117
msgid ""
"If you can dedicate more time, you could manage a piece of the Free Software "
"collection within Debian.  Especially helpful is if people adopt or maintain "
"items that people have requested for inclusion within Debian.  The <ulink "
"url=\"&url-wnpp;\">Work Needing and Prospective Packages database</ulink> "
"details this information.  If you have an interest in specific groups then "
"you may find enjoyment in contributing to some of Debian's <ulink url=\"&url-"
"debian-projects;\">subprojects</ulink> which include ports to particular "
"architectures and <ulink url=\"&url-debian-blends;\">Debian Pure Blends</"
"ulink> for specific user groups, among many others."
msgstr ""
"Om du kan avsätta mer tid, skulle du kunna ansvara för en del av den fria "
"programvaran i Debian. Speciellt behjälpligt är det om personer adopterar "
"eller ansvarar för saker som folk har frågat efter om att inkluderas i "
"Debian. Databasen <ulink url=\"&url-wnpp;\">Work Needing and Prospective "
"Packages</ulink> har detaljer om detta. Om du har intresse av mer specifika "
"grupper kan du finna glädje i att bidra till några av Debians <ulink url="
"\"&url-debian-projects;\">underprojekt</ulink> vilka inkluderar porteringar "
"till specifika arkitekturer, <ulink url=\"&url-debian-blends;\">Debian Pure "
"Blends</ulink> för specifika användargrupper bland många andra."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:128
msgid ""
"In any case, if you are working in the free software community in any way, "
"as a user, programmer, writer, or translator you are already helping the "
"free software effort.  Contributing is rewarding and fun, and as well as "
"allowing you to meet new people it gives you that warm fuzzy feeling inside."
msgstr ""
"I vilket fall som helst, om du arbetar i den fria programvarugemenskapen på "
"något sätt, som en användare, programmerare, författare eller översättare "
"hjälper du redan den fria programvaran. Att bidra är belönande och roligt, "
"såväl som att det låter dig träffa nya människor som att det ger dig den där "
"varma känslan inom dig."
