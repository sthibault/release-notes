# Slovak messages for release-notes.
# Copyright (C) 2009 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Ivan Masár <helix84@centrum.sk>, 2009, 2011, 2013, 2015, 2017.
msgid ""
msgstr ""
"Project-Id-Version: release-notes 5.0\n"
"POT-Creation-Date: 2017-06-16 23:30+0200\n"
"PO-Revision-Date: 2017-06-16 15:21+0200\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: x\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Virtaal 0.7.1\n"

#. type: Attribute 'lang' of: <appendix>
#: en/old-stuff.dbk:8
msgid "en"
msgstr "sk"

#. type: Content of: <appendix><title>
#: en/old-stuff.dbk:9
msgid "Managing your &oldreleasename; system before the upgrade"
msgstr "Ako spravovať váš systém &oldreleasename; pred aktualizáciou"

#. type: Content of: <appendix><para>
#: en/old-stuff.dbk:11
msgid ""
"This appendix contains information on how to make sure you can install or "
"upgrade &oldreleasename; packages before you upgrade to &releasename;.  This "
"should only be necessary in specific situations."
msgstr ""
"Táto príloha obsahuje informácie o tom, ako sa môžete uistiť, že dokážete "
"inštalovať a aktualizovať balíky &oldreleasename; pred aktualizáciou na "
"&releasename;. Malo by to byť potrebné iba v určitých situáciách."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:16
msgid "Upgrading your &oldreleasename; system"
msgstr "Ako aktualizovať váš systém &oldreleasename;"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:18
msgid ""
"Basically this is no different from any other upgrade of &oldreleasename; "
"you've been doing.  The only difference is that you first need to make sure "
"your package list still contains references to &oldreleasename; as explained "
"in <xref linkend=\"old-sources\"/>."
msgstr ""
"V podstate sa to nelíši od bežnej aktualizácie &oldreleasename;, akú ste "
"vykonávali doteraz. Jediný rozdiel je v tom, že sa musíte uistiť, že váš "
"zoznam balíkov ešte stále obsahuje odkazy na &oldreleasename;, ako "
"vysvetľuje <xref linkend=\"old-sources\"/>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:24
msgid ""
"If you upgrade your system using a Debian mirror, it will automatically be "
"upgraded to the latest &oldreleasename; point release."
msgstr ""
"Ak aktualizujete svoj systém pomocou zrkadla Debianu, bude automaticky "
"aktualizovaný na najnovšiu aktualizáciu stabilnej vetvy (point release) "
"&oldreleasename;."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:30
msgid "Checking your sources list"
msgstr "Ako skontrolovať váš zoznam zdrojov"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:32
msgid ""
"If any of the lines in your <filename>/etc/apt/sources.list</filename> refer "
"to <quote><literal>stable</literal></quote>, it effectively points to "
"&releasename; already. This might not be what you want if you are not ready "
"yet for the upgrade.  If you have already run <command>apt-get update</"
"command>, you can still get back without problems by following the procedure "
"below."
msgstr ""
"Ak ktorýkoľvek z riadkov vo vašom <filename>/etc/apt/sources.list</filename> "
"odkazuje na <quote><literal>stable</literal></quote>, už vlastne na "
"&releasename; ukazuje. To nemusí byť to, čo ste mali v úmysle, ak zatiaľ nie "
"ste na aktualizáciu pripravený. Ak ste už spustili <command>apt-get update</"
"command>, ešte stále sa môžete vrátiť späť bez problémov pomocou nasledovnej "
"procedúry."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:40
msgid ""
"If you have also already installed packages from &releasename;, there "
"probably is not much point in installing packages from &oldreleasename; "
"anymore.  In that case you will have to decide for yourself whether you want "
"to continue or not.  It is possible to downgrade packages, but that is not "
"covered here."
msgstr ""
"Ak ste už naviac nainštalovali balíky z &releasename;, už pravdepodobne nemá "
"zmysel inštalovať  balíky z &oldreleasename;. V tom prípade sa budete musieť "
"sami rozhodnúť či chcete pokračovať alebo nie. Je možné znížiť verziu "
"balíkov, ale to tento dokument nepopisuje."

#. type: Content of: <appendix><section><para><footnote><para>
#: en/old-stuff.dbk:52
msgid ""
"<ulink url=\"https://lists.debian.org/debian-announce/2017/msg00001.html"
"\">Debian will remove FTP access to all of its official mirrors on "
"2017-11-01</ulink>.  If your sources.list contains a <literal>debian.org</"
"literal> host, please consider switching to <ulink url=\"https://deb.debian."
"org\">deb.debian.org</ulink>.  This note only applies to mirrors hosted by "
"Debian itself.  If you use a secondary mirror or a third-party repository, "
"then they may still support FTP access after that date.  Please consult with "
"the operators of these if you are in doubt."
msgstr ""
"<ulink url=\"https://lists.debian.org/debian-announce/2017/msg00001.html"
"\">Debian odstráni FTP prístup na všetky oficiálne zrkadlá dňa 2017-11-01</"
"ulink>.  Ak váš súbor sources.list obsahuje počítač <literal>debian.org</"
"literal>, prosím, zvážte jeho zmenu na <ulink url=\"https://deb.debian.org"
"\">deb.debian.org</ulink>. Táto poznámka sa vzťahuje iba na zrkadlá, ktoré "
"hosťuje samotný projekt Debian. Ak používate sekundárne zrkadlo alebo "
"úložisko, ktoré prevádzkuje tretia strana, tá môže aj tomto dátume naďalej "
"podporovať prístup prostredníctvom FTP. Ak máte pochybnosti, obráťte sa na "
"jeho prevádzkovateľa."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:46
msgid ""
"Open the file <filename>/etc/apt/sources.list</filename> with your favorite "
"editor (as <literal>root</literal>) and check all lines beginning with "
"<literal>deb http:</literal>, <literal>deb https:</literal>, <literal>deb tor"
"+http:</literal>, <literal>deb tor+https:</literal> or <literal>deb ftp:</"
"literal><placeholder type=\"footnote\" id=\"0\"/> for a reference to "
"<quote><literal>stable</literal></quote>.  If you find any, change "
"<literal>stable</literal> to <literal>&oldreleasename;</literal>."
msgstr ""
"Otvorte súbor <filename>/etc/apt/sources.list</filename> vo svojom obľúbenom "
"editore (ako <literal>root</literal>) a skontrolujte všetky riadky "
"začínajúce <literal>deb http:</literal>, <literal>deb https:</literal>, "
"<literal>deb tor+http:</literal>, <literal>deb tor+https:</literal> alebo "
"<literal>deb ftp:</literal><placeholder type=\"footnote\" id=\"0\"/> "
"obsahujúce <quote><literal>stable</literal></quote>. Ak nejaké nájdete, "
"zmeňte ich zo <literal>stable</literal> na <literal>&oldreleasename;</"
"literal>."

#. type: Content of: <appendix><section><note><para>
#: en/old-stuff.dbk:69
msgid ""
"Lines in sources.list starting with <quote>deb ftp:</quote> and pointing to "
"debian.org addresses should be changed into <quote>deb http:</quote> lines.  "
"See <xref linkend=\"deprecation-of-ftp-apt-mirrors\" />."
msgstr ""
"Riadky v sources.list začínajúce na <quote>deb ftp:</quote> a smerujúce na "
"adresy debian.org by ste mali zmeniť na riadky <quote>deb http:</quote>. "
"Pozri <xref linkend=\"deprecation-of-ftp-apt-mirrors\" />."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:75
msgid ""
"If you have any lines starting with <literal>deb file:</literal>, you will "
"have to check for yourself if the location they refer to contains an "
"&oldreleasename; or a &releasename; archive."
msgstr ""
"Ak máte nejaké riadky začínajúce <literal>deb file:</literal>, musíte sami "
"skontrolovať, či miesto kam odkazujú obsahuje archív &oldreleasename; alebo "
"archív &releasename;."

#. type: Content of: <appendix><section><important><para>
#: en/old-stuff.dbk:81
msgid ""
"Do not change any lines that begin with <literal>deb cdrom:</literal>.  "
"Doing so would invalidate the line and you would have to run <command>apt-"
"cdrom</command> again.  Do not be alarmed if a <literal>cdrom:</literal> "
"source line refers to <quote><literal>unstable</literal></quote>.  Although "
"confusing, this is normal."
msgstr ""
"Nemeňte žiadne riadky začínajúce <literal>deb cdrom:</literal>. Tým by ste "
"riadok zneplatnili a museli by ste znova spustiť <command>apt-cdrom</"
"command>. Neznepokojujte sa ak riadok so zdrojom <literal>cdrom:</literal> "
"odkazuje na <quote><literal>unstable</literal></quote>. Hoci to môže byť "
"mätúce, je to v poriadku."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:89
msgid "If you've made any changes, save the file and execute"
msgstr "Ak ste vykonali nejaké zmeny, uložte súbor a spustite"

#. type: Content of: <appendix><section><screen>
#: en/old-stuff.dbk:92
#, no-wrap
msgid "# apt-get update\n"
msgstr "# apt-get update\n"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:95
msgid "to refresh the package list."
msgstr "aby sa aktualizoval zoznam balíkov."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:100
msgid "Removing obsolete configuration files"
msgstr "Odstránenie zastaralých konfiguračných súborov"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:102
msgid ""
"Before upgrading your system to &releasename;, it is recommended to remove "
"old configuration files (such as <filename>*.dpkg-{new,old}</filename> files "
"under <filename>/etc</filename>) from the system."
msgstr ""
"Pred aktualizáciou systému na &releasename; sa odporúča odstrániť zo systému "
"staré konfiguračné súbory (napríklad súbory <filename>*.dpkg-{new,old}</"
"filename> súbory v <filename>/etc</filename>)."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:110
msgid "Upgrade legacy locales to UTF-8"
msgstr "Aktualizácia starých locales na UTF-8"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:112
msgid ""
"Using a legacy non-UTF-8 locale has been unsupported by desktops and other "
"mainstream software projects for a long time. Such locales should be "
"upgraded by running <command>dpkg-reconfigure locales</command> and "
"selecting a UTF-8 default. You should also ensure that users are not "
"overriding the default to use a legacy locale in their environment."
msgstr ""
"Používanie staršieho národného prostredia (locale), ktoré nie je UTF-8, už "
"dlhší čas nepodporujú pracovné prostredia a iné bežné softvérové projekty. "
"Takéto národné prostredia by ste mali aktualizovať spustením príkazu "
"<command>dpkg-reconfigure locales</command> a vybraním predvoleného UTF-8. "
"Takisto by ste mali zabezpečiť, aby používatelia neprekonávali predvolené "
"nastavenia späť na zastaralé národné prostredie vo svojom prostredí."

#~ msgid ""
#~ "In the GNOME screensaver, using passwords with non-ASCII characters, "
#~ "pam_ldap support, or even the ability to unlock the screen may be "
#~ "unreliable when not using UTF-8.  The GNOME screenreader is affected by "
#~ "bug <ulink url=\"http://bugs.debian.org/599197\">#599197</ulink>.  The "
#~ "Nautilus file manager (and all glib-based programs, and likely all Qt-"
#~ "based programs too) assume that filenames are in UTF-8, while the shell "
#~ "assumes they are in the current locale's encoding. In daily use, non-"
#~ "ASCII filenames are just unusable in such setups.  Furthermore, the gnome-"
#~ "orca screen reader (which grants sight-impaired users access to the GNOME "
#~ "desktop environment) requires a UTF-8 locale since Squeeze; under a "
#~ "legacy characterset, it will be unable to read out window information for "
#~ "desktop elements such as Nautilus/GNOME Panel or the Alt-F1 menu."
#~ msgstr ""
#~ "V šetriči obrazovky GNOME, ak nepoužívate UTF-8, používanie hesiel s ne-"
#~ "ASCII znakmi, podpora pam_ldap alebo dokonca schopnosť odomknúť obrazovku "
#~ "môžu byť nespoľahlivé. Čítačka obrazovky GNOME trpí chybou <ulink url="
#~ "\"http://bugs.debian.org/599197\">#599197</ulink>. Správca súborov "
#~ "Nautilus (a všetky programy založené na glib, a pravdepodobne tiež všetky "
#~ "programy založené na Qt) predpokladajú, že názvy súborov sú v UTF-8, "
#~ "zatiaľčo shell predpokladá, že sú v kódovaní podľa aktuálne nastaveného "
#~ "locale. Ne-ASCII znaky v názvov sú tak na každodenné použitie v takomto "
#~ "prostredí prakticky nepoužiteľné. Navyše čítačka obrazovky gnome-orca "
#~ "(ktorá sprístupňuje zrakovo postihnutým používateľom prístup k pracovnému "
#~ "prostrediu GNOME) vyžaduje od vydania Squeeze locale UTF-8; pri použití "
#~ "staršej znakovej sady, nebude schopná prečítať informácie okien prvkov "
#~ "pracovnej plochy ako Nautilus/GNOME Panel alebo ponuku LeftAlt-F1."

#~ msgid ""
#~ "If your system is localized and is using a locale that is not based on "
#~ "UTF-8 you should strongly consider switching your system over to using "
#~ "UTF-8 locales.  In the past, there have been bugs<placeholder type="
#~ "\"footnote\" id=\"0\"/> identified that manifest themselves only when "
#~ "using a non-UTF-8 locale. On the desktop, such legacy locales are "
#~ "supported through ugly hacks in the library internals, and we cannot "
#~ "decently provide support for users who still use them."
#~ msgstr ""
#~ "Ak je váš systém lokalizovaný a používa locale, ktoré nie je založené na "
#~ "UTF-8, mali by ste silne zvážiť zmenu locales vášho systému na UTF-8. V "
#~ "minulosti boli identifikované chyby<placeholder type=\"footnote\" id="
#~ "\"0\"/>, ktoré sa prejavujú iba pri locale, ktoré nie sú založené na "
#~ "UTF-8. Na systéme s pracovným prostredím sú takéto staré locales "
#~ "podporované iba škaredými kľučkami vnútri knižníc a nedokážeme kvalitne "
#~ "podporovať používateľov, ktorí ich ešte používajú."

#~ msgid ""
#~ "To configure your system's locale you can run <command>dpkg-reconfigure "
#~ "locales</command>. Ensure you select a UTF-8 locale when you are "
#~ "presented with the question asking which locale to use as a default in "
#~ "the system.  In addition, you should review the locale settings of your "
#~ "users and ensure that they do not have legacy locale definitions in their "
#~ "configuration environment."
#~ msgstr ""
#~ "Locale svojho systému môžete nastaviť spustením <command>dpkg-reconfigure "
#~ "locales</command>. Uistite sa, že ste zvolili UTF-8 locale, pri výbere "
#~ "predvoleného locale systému. Okrem toho by ste mali skontrolovať "
#~ "nastavenia locale svojich používateľov a ubezpečiť sa, že vo svojej "
#~ "konfigurácii nepoužívajú staré locales."

#~ msgid ""
#~ "Since release 2:1.7.7-12, xorg-server no longer reads the file "
#~ "XF86Config-4.  See also <ulink url=\"http://bugs.debian."
#~ "org/619177\">#619177</ulink>."
#~ msgstr ""
#~ "Od vydania 2:1.7.7-12 xorg server už nečíta súbor XF86Config-4. Pozri aj "
#~ "<ulink url=\"http://bugs.debian.org/619177\">#619177</ulink>."
