# Vietnamese translation for Release Notes (More Info).
# Copyright © 2009 Free Software Foundation, Inc.
# Clytie Siddall <clytie@riverland.net.au>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 5.0\n"
"POT-Creation-Date: 2017-01-26 23:48+0100\n"
"PO-Revision-Date: 2009-02-04 17:56+1030\n"
"Last-Translator: Clytie Siddall <clytie@riverland.net.au>\n"
"Language-Team: Vietnamese <vi-VN@googlegroups.com>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: LocFactoryEditor 1.8\n"

#. type: Attribute 'lang' of: <chapter>
#: en/moreinfo.dbk:8
msgid "en"
msgstr "vi"

#. type: Content of: <chapter><title>
#: en/moreinfo.dbk:9
msgid "More information on &debian;"
msgstr "Thông tin thêm về &debian;"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:11
msgid "Further reading"
msgstr "Đọc thêm"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:13
#, fuzzy
#| msgid ""
#| "Beyond these release notes and the installation guide, further "
#| "documentation on &debian; is available from the Debian Documentation "
#| "Project (DDP), whose goal is to create high-quality documentation for "
#| "Debian users and developers.  Documentation, including the Debian "
#| "Reference, Debian New Maintainers Guide, and Debian FAQ are available, "
#| "and many more.  For full details of the existing resources see the <ulink "
#| "url=\"&url-ddp;\">DDP website</ulink>."
msgid ""
"Beyond these release notes and the installation guide, further documentation "
"on &debian; is available from the Debian Documentation Project (DDP), whose "
"goal is to create high-quality documentation for Debian users and "
"developers.  Available documentation includes the Debian Reference, Debian "
"New Maintainers Guide, the Debian FAQ, and many more.  For full details of "
"the existing resources see the <ulink url=\"&url-ddp;\">Debian Documentation "
"website</ulink> and the <ulink url=\"&url-wiki;\">Debian Wiki website</"
"ulink>."
msgstr ""
"Ra khỏi Ghi chú Phát hành này và Sổ tay Cài đặt, tài liệu hướng dẫn về "
"&debian; sẵn sàng từ Dự án Tài liệu Debian (Debian Documentation Project: "
"DDP) mà có mục đích tạo tài liệu hướng dẫn có chất lượng cao cho các người "
"dùng và nhà phát triển Debian. Tài liệu hướng dẫn, bao gồm Debian Reference "
"(Tham chiếu Debian), Debian New Maintainers Guide (Sổ tay Nhà duy trì Mới) "
"và Debian FAQ (Hỏi Đáp) cũng sẵn sàng (tiếc là chưa dịch sang tiếng Việt) và "
"rất nhiều tài liệu hữu ích bổ sung. Để tìm chi tiết về những tài nguyên sẵn "
"sàng, xem <ulink url=\"&url-ddp;\">Địa chỉ Web của DDP</ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:23
#, fuzzy
#| msgid ""
#| "Documentation for individual packages is installed into <filename>/usr/"
#| "share/doc/<replaceable>package</replaceable></filename>.  This may "
#| "include copyright information, Debian specific details and any upstream "
#| "documentation."
msgid ""
"Documentation for individual packages is installed into <filename>/usr/share/"
"doc/<replaceable>package</replaceable></filename>.  This may include "
"copyright information, Debian specific details, and any upstream "
"documentation."
msgstr ""
"Tài liệu hướng dẫn về mỗi gói riêng được cài đặt vào thư mục <filename>/usr/"
"share/doc/<replaceable>tên_gói</replaceable></filename>. Các tài liệu này có "
"thể bao gồm thông tin tác quyền, chi tiết đặc trưng cho Debian, và tài liệu "
"hướng dẫn của nhà phát triển gốc. "

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:31
msgid "Getting help"
msgstr "Tìm trợ giúp"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:33
#, fuzzy
#| msgid ""
#| "There are many sources of help, advice and support for Debian users, but "
#| "these should only be considered if research into documentation of the "
#| "issue has exhausted all sources.  This section provides a short "
#| "introduction into these which may be helpful for new Debian users."
msgid ""
"There are many sources of help, advice, and support for Debian users, but "
"these should only be considered if research into documentation of the issue "
"has exhausted all sources.  This section provides a short introduction to "
"these sources which may be helpful for new Debian users."
msgstr ""
"Có rất nhiều nguồn trợ giúp, lời khuyên và hỗ trợ cho người dùng Debian, "
"nhưng chỉ nên xem xét nguồn như vậy nếu tài liệu hướng dẫn có sẵn không giúp "
"người dùng giải quyết vấn đề. Phần này giới thiệu ngắn những nguồn trợ giúp "
"bổ sung có thể hữu ích cho người dùng Debian vẫn bắt đầu."

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:39
msgid "Mailing lists"
msgstr "Hộp thư chung"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:41
msgid ""
"The mailing lists of most interest to Debian users are the debian-user list "
"(English) and other debian-user-<replaceable>language</replaceable> lists "
"(for other languages).  For information on these lists and details of how to "
"subscribe see <ulink url=\"&url-debian-list-archives;\"></ulink>.  Please "
"check the archives for answers to your question prior to posting and also "
"adhere to standard list etiquette."
msgstr ""
"Những hộp thư chung (mailing list) hữu ích nhất cho người dùng Debian là « "
"debian-user » (tiếng Anh: cũng có thể yêu cầu dự án tạo một hộp thư chung "
"người dùng bằng ngôn ngữ mẹ đẻ). Để tìm thông tin về các hộp thư chung "
"Debian và cách đăng ký, xem <ulink url=\"&url-debian-list-archives;\"></"
"ulink>. Trước khi gửi một thư xin trợ giúp, người dùng nên tìm kiếm qua kho "
"thư (mà chứa rất nhiều thông tin về vấn đề thường gặp), và tùy theo quy ước "
"mặc nhận trong hộp thư chung (v.d. hãy lặng lẽ, viết đáp ứng bên dưới thân "
"thư gốc, cắt ngắn đoạn văn gốc không còn có ích lại, không nên VIẾT CẢ CHỮ "
"HOA, không đính kèm tập tin (gửi tập tin đính kèm một thư riêng cho người "
"dùng khác), thay đổi dòng Chủ đề khi giới thiệu một vấn đề mới)."

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:51
msgid "Internet Relay Chat"
msgstr "IRC — Trò chuyện chuyển tiếp trên Internet"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:53
#, fuzzy
#| msgid ""
#| "Debian has an IRC channel dedicated to the support and aid of Debian "
#| "users located on the OFTC IRC network.  To access the channel, point your "
#| "favorite IRC client at irc.debian.org and join <literal>#debian</literal>."
msgid ""
"Debian has an IRC channel dedicated to the support and aid of Debian users, "
"located on the OFTC IRC network.  To access the channel, point your favorite "
"IRC client at irc.debian.org and join <literal>#debian</literal>."
msgstr ""
"Debian có một kênh IRC dành cho hỗ trợ và giúp đỡ các người dùng Debian, "
"chạy trên mạng IRC OFTC. Để truy cập đến kênh này, hãy chỉ ứng dụng khách "
"IRC (v.d. XChat) tới địa chỉ « irc.debian.org » và vào kênh « "
"<literal>#debian</literal> »."

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:58
#, fuzzy
#| msgid ""
#| "Please follow the channel guidelines, respecting other users fully.  The "
#| "guidelines are available at the <ulink url=\"&url-wiki;DebianIRC\">Debian "
#| "Wiki</ulink>."
msgid ""
"Please follow the channel guidelines, respecting other users fully.  The "
"guidelines are available at the <ulink url=\"&url-wiki;DebianIRC\">Debian "
"Wiki</ulink>."
msgstr ""
"Hãy tùy theo các hướng dẫn về sử dụng kênh (quan trọng nhất là lặng lẽ) mà "
"sẵn sàng trong <ulink url=\"&url-wiki;DebianIRC\">Debian Wiki</ulink>."

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:63
msgid ""
"For more information on OFTC please visit the <ulink url=\"&url-irc-host;"
"\">website</ulink>."
msgstr ""
"Để tìm thêm thông tin về mạng OFTC, xem <ulink url=\"&url-irc-host;\">địa "
"chỉ Web</ulink> này."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:71
msgid "Reporting bugs"
msgstr "Thông báo lỗi"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:73
#, fuzzy
#| msgid ""
#| "We strive to make &debian; a high quality operating system, however that "
#| "does not mean that the packages we provide are totally free of bugs.  "
#| "Consistent with Debian's <quote>open development</quote> philosophy and "
#| "as a service to our users, we provide all the information on reported "
#| "bugs at our own Bug Tracking System (BTS).  The BTS is browseable at "
#| "<ulink url=\"&url-bts;\"></ulink>."
msgid ""
"We strive to make &debian; a high quality operating system; however that "
"does not mean that the packages we provide are totally free of bugs.  "
"Consistent with Debian's <quote>open development</quote> philosophy and as a "
"service to our users, we provide all the information on reported bugs at our "
"own Bug Tracking System (BTS).  The BTS is browseable at <ulink url=\"&url-"
"bts;\"></ulink>."
msgstr ""
"Chúng tôi cố gắng làm cho &debian; là một hệ điều hành có chất lượng cao, "
"tuy nhiên đó không có nghĩa là mọi gói đều hoàn toàn miễn lỗi. Tùy theo ý "
"kiến <quote>phát triển mở</quote> của dự án Debian, và để giúp đỡ các người "
"dùng, chúng tôi cung cấp tất cả các thông tin về những lỗi đã thông báo, "
"thông qua Hệ thống Theo dõi Lỗi (BTS): có thể duyệt qua nó ở <ulink url="
"\"&url-bts;\"></ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:81
#, fuzzy
#| msgid ""
#| "If you find a bug in the distribution or in packaged software that is "
#| "part of it, please report it so that it can be properly fixed for future "
#| "releases.  Reporting bugs requires a valid email address.  We ask for "
#| "this so that we can trace bugs and developers can get in contact with "
#| "submitters should additional information be needed."
msgid ""
"If you find a bug in the distribution or in packaged software that is part "
"of it, please report it so that it can be properly fixed for future "
"releases.  Reporting bugs requires a valid e-mail address.  We ask for this "
"so that we can trace bugs and developers can get in contact with submitters "
"should additional information be needed."
msgstr ""
"Nếu người dùng gặp một lỗi trong bản phân phối, hoặc trong phần mềm đã đóng "
"gói thuộc về nó, hãy thông báo nó để cho phép nhà phát triển sửa chữa trong "
"bản phát hành sau. Để thông báo lỗi, cũng cần nhập một địa chỉ thư điện tử "
"vẫn hoạt động. Địa chỉ này cần thiết để giúp nhà phát triển yêu cầu thêm "
"thông tin, và theo dõi mẫu của các lỗi hiện thời."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:88
#, fuzzy
#| msgid ""
#| "You can submit a bug report using the program <command>reportbug</"
#| "command> or manually using email.  You can read more about the Bug "
#| "Tracking System and how to use it by reading the reference documentation "
#| "(available at <filename>/usr/share/doc/debian</filename> if you have "
#| "<systemitem role=\"package\">doc-debian</systemitem> installed) or online "
#| "at the <ulink url=\"&url-bts;\">Bug Tracking System</ulink>."
msgid ""
"You can submit a bug report using the program <command>reportbug</command> "
"or manually using e-mail.  You can read more about the Bug Tracking System "
"and how to use it by reading the reference documentation (available at "
"<filename>/usr/share/doc/debian</filename> if you have <systemitem role="
"\"package\">doc-debian</systemitem> installed) or online at the <ulink url="
"\"&url-bts;\">Bug Tracking System</ulink>."
msgstr ""
"Người dùng lúc nào cũng có khả năng tự động gửi báo cáo lỗi dùng chương "
"trình <command>reportbug</command> hoặc tự gửi một báo cáo bằng thư điện tử. "
"Có thêm thông tin về Hệ thống Theo dõi Lỗi và cách sử dụng nó trong tài liệu "
"tham chiếu (có sẵn trong thư mục <filename>/usr/share/doc/debian</filename> "
"nếu người dùng đã cài đặt gói <systemitem role=\"package\">doc-debian</"
"systemitem>) hoặc trên Internet ở <ulink url=\"&url-bts;\">Hệ thống Theo dõi "
"Lỗi</ulink>."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:98
msgid "Contributing to Debian"
msgstr "Đóng góp cho Debian"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:100
#, fuzzy
#| msgid ""
#| "You do not need to be an expert to contribute to Debian.  By assisting "
#| "users with problems on the various user support <ulink url=\"&url-debian-"
#| "list-archives;\">lists</ulink> you are contributing to the community.  "
#| "Identifying (and also solving) problems related to the development of the "
#| "distribution by participating on the development <ulink url=\"&url-debian-"
#| "list-archives;\">lists</ulink> is also extremely helpful.  To maintain "
#| "Debian's high quality distribution, <ulink url=\"&url-bts;\">submit bugs</"
#| "ulink> and help developers track them down and fix them.  If you have a "
#| "way with words then you may want to contribute more actively by helping "
#| "to write <ulink url=\"&url-ddp;\">documentation</ulink> or <ulink url="
#| "\"&url-debian-i18n;\">translate</ulink> existing documentation into your "
#| "own language."
msgid ""
"You do not need to be an expert to contribute to Debian.  By assisting users "
"with problems on the various user support <ulink url=\"&url-debian-list-"
"archives;\">lists</ulink> you are contributing to the community.  "
"Identifying (and also solving) problems related to the development of the "
"distribution by participating on the development <ulink url=\"&url-debian-"
"list-archives;\">lists</ulink> is also extremely helpful.  To maintain "
"Debian's high quality distribution, <ulink url=\"&url-bts;\">submit bugs</"
"ulink> and help developers track them down and fix them.  The tool "
"<systemitem role=\"package\">how-can-i-help</systemitem> helps you to find "
"suitable reported bugs to work on.  If you have a way with words then you "
"may want to contribute more actively by helping to write <ulink url=\"&url-"
"ddp-svn-info;\">documentation</ulink> or <ulink url=\"&url-debian-i18n;"
"\">translate</ulink> existing documentation into your own language."
msgstr ""
"Để đóng góp cho Debian, người dùng không cần là chuyên gia về vi tính. Cũng "
"có thể đóng góp cho cộng đồng bằng cách giúp người dùng xin trợ giúp trong "
"các <ulink url=\"&url-debian-list-archives;\">hộp thư chung</ulink> hỗ trợ "
"người dùng. Trong <ulink url=\"&url-debian-list-archives;\">hộp thư chung</"
"ulink> kiểu phát triển, mỗi người tham gia cũng rất giúp ích. Bản phân phối "
"Debian chỉ bảo tồn mức chất lượng khi người dùng <ulink url=\"&url-bts;"
"\">thông báo lỗi</ulink> được phát hiện, ngay cả giúp nhà phát triển tìm vết "
"và sửa chữa lỗi. Cũng có thể giúp viết <ulink url=\"&url-ddp;\">tài liệu</"
"ulink> gốc hoặc <ulink url=\"&url-debian-i18n;\">dịch</ulink> tài liệu đã "
"tồn tại sang tiếng Việt (tham gia nhé :) )."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:117
#, fuzzy
#| msgid ""
#| "If you can dedicate more time, you could manage a piece of the Free "
#| "Software collection within Debian.  Especially helpful is if people adopt "
#| "or maintain items that people have requested for inclusion within "
#| "Debian.  The <ulink url=\"&url-wnpp;\">Work Needing and Prospective "
#| "Packages database</ulink> details this information.  If you have an "
#| "interest in specific groups then you may find enjoyment in contributing "
#| "to some of Debian's subprojects which include ports to particular "
#| "architectures, <ulink url=\"&url-debian-jr;\">Debian Jr.</ulink> and "
#| "<ulink url=\"&url-debian-med;\">Debian Med</ulink>."
msgid ""
"If you can dedicate more time, you could manage a piece of the Free Software "
"collection within Debian.  Especially helpful is if people adopt or maintain "
"items that people have requested for inclusion within Debian.  The <ulink "
"url=\"&url-wnpp;\">Work Needing and Prospective Packages database</ulink> "
"details this information.  If you have an interest in specific groups then "
"you may find enjoyment in contributing to some of Debian's <ulink url=\"&url-"
"debian-projects;\">subprojects</ulink> which include ports to particular "
"architectures and <ulink url=\"&url-debian-blends;\">Debian Pure Blends</"
"ulink> for specific user groups, among many others."
msgstr ""
"Nếu người dùng có đủ thời gian rảnh, cũng có thể quản lý một hay vài gói "
"phần mềm tự do bên trong Debian. Đặc biệt giúp ích khi một người tham gia "
"cũng chịu trách nhiệm về hay duy trì phần mềm được người dùng yêu cầu bao "
"gồm trong Debian. <ulink url=\"&url-wnpp;\">Cơ sở dữ liệu Việc cần làm và "
"Gói tương lai</ulink> (Work Needing and Prospective Packages) chứa thông tin "
"đó. Người dùng quan tâm đến một vùng riêng cũng có thể đóng góp cho dự án "
"con của Debian, v.d. chuyển phần mềm sang kiến trúc riêng, <ulink url=\"&url-"
"debian-jr;\">Debian Trẻ</ulink> và <ulink url=\"&url-debian-med;\">Y tế "
"Debian</ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:128
#, fuzzy
#| msgid ""
#| "In any case, if you are working in the free software community in any "
#| "way, as a user, programmer, writer or translator you are already helping "
#| "the free software effort.  Contributing is rewarding and fun, and as well "
#| "as allowing you to meet new people it gives you that warm fuzzy feeling "
#| "inside."
msgid ""
"In any case, if you are working in the free software community in any way, "
"as a user, programmer, writer, or translator you are already helping the "
"free software effort.  Contributing is rewarding and fun, and as well as "
"allowing you to meet new people it gives you that warm fuzzy feeling inside."
msgstr ""
"Trong mọi trường hợp đều, nếu người dùng tham gia cộng đồng phần mềm tự do "
"bằng cách nào cả, là một người dùng, nhà phát triển, tác giả hay người dịch, "
"thì vẫn còn đóng góp cho sự cố gắng phần mềm tự do. Đóng góp vẫn đáng làm và "
"thứ vị, cũng cho phép người dùng kết bạn và học hiểu thêm."
