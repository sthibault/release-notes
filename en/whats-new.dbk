<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
  "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % languagedata SYSTEM "language.ent" > %languagedata;
  <!ENTITY % shareddata   SYSTEM "../release-notes.ent" > %shareddata;
]>

<chapter id="ch-whats-new" lang="en">
<title>What's new in &debian; &release;</title>
<para>
  The <ulink url="&url-wiki-newinrelease;">Wiki</ulink> has more information
about this topic.
</para>

<!--
Sources for architecture status:
  https://release.debian.org/buster/arch_qualify.html

Some descriptions of the ports: https://www.debian.org/ports/
-->
<section>
<title>Supported architectures</title>

<para>
The following are the officially supported architectures for &debian;
&release;:
</para>
<itemizedlist>
<listitem>
<para>
32-bit PC (<literal>i386</literal>) and 64-bit PC (<literal>amd64</literal>)
</para>
</listitem>
<listitem>
<para>
64-bit ARM (<literal>arm64</literal>)
</para>
</listitem>
<listitem>
<para>
ARM EABI (<literal>armel</literal>)
</para>
</listitem>
<listitem>
<para>
ARMv7 (EABI hard-float ABI, <literal>armhf</literal>)
</para>
</listitem>
<listitem>
<para>
MIPS (<literal>mips</literal> (big-endian) and <literal>mipsel</literal> (little-endian))
</para>
</listitem>
<listitem>
<para>
64-bit little-endian MIPS (<literal>mips64el</literal>)
</para>
</listitem>
<listitem>
<para>
64-bit little-endian PowerPC (<literal>ppc64el</literal>)
</para>
</listitem>
<listitem>
<para>
IBM System z (<literal>s390x</literal>)
</para>
</listitem>
</itemizedlist>

<para>
You can read more about port status, and port-specific information for your
architecture at the <ulink url="&url-ports;">Debian port
web pages</ulink>.
</para>

</section>

<section id="newdistro">
<title>What's new in the distribution?</title>

<programlisting condition="fixme">
 TODO: Make sure you update the numbers in the .ent file
     using the changes-release.pl script found under ../
</programlisting>

<para>
This new release of Debian again comes with a lot more software than
its predecessor &oldreleasename;; the distribution includes over
&packages-new; new packages, for a total of over &packages-total;
packages.  Most of the software in the distribution has been updated:
over &packages-updated; software packages (this is
&packages-update-percent;% of all packages in &oldreleasename;).
Also, a significant number of packages (over &packages-removed;,
&packages-removed-percent;% of the packages in &oldreleasename;) have
for various reasons been removed from the distribution.  You will not
see any updates for these packages and they will be marked as
"obsolete" in package management front-ends; see <xref
linkend="obsolete"/>.
</para>

<para>
  &debian; again ships with several desktop applications and
  environments.  Among others it now includes the desktop environments
  GNOME<indexterm><primary>GNOME</primary></indexterm> 3.22,
  KDE Plasma<indexterm><primary>KDE</primary></indexterm> 5.8,
  LXDE<indexterm><primary>LXDE</primary></indexterm>,
  LXQt<indexterm><primary>LXQt</primary></indexterm> 0.11,
  MATE<indexterm><primary>MATE</primary></indexterm> 1.16, and
  Xfce<indexterm><primary>Xfce</primary></indexterm> 4.12.
</para>
<para>
  Productivity applications have also been upgraded, including the
  office suites:
</para>
  <itemizedlist>
  <listitem>
    <para>
      LibreOffice<indexterm><primary>LibreOffice</primary></indexterm>
      is upgraded to version 5.2;
    </para>
  </listitem>
  <listitem>
    <para>
      Calligra<indexterm><primary>Calligra</primary></indexterm>
      is upgraded to 2.9.
    </para>
  </listitem>
  <!-- no updates?
  <listitem>
    <para>
      GNUcash<indexterm><primary>GNUcash</primary></indexterm> is upgraded to 2.6;
    </para>
  </listitem>
  <listitem>
    <para>
      GNUmeric<indexterm><primary>GNUmeric</primary></indexterm> is upgraded to 1.12;
    </para>
  </listitem>
  <listitem>
    <para>
      Abiword<indexterm><primary>Abiword</primary></indexterm> is upgraded to 3.0.
      </listitem>
      </ -->
  </itemizedlist>

<para>
  With &releasename;, Debian for the first time brings a mandatory access
  control framework enabled per default. New installations of &debian;
  &releasename; will have AppArmor installed and enabled per default. See
  below for more information.
</para>

<para>
  Besides, buster is the first Debian release to ship with Rust based programs
  such as Firefox, ripgrep, fd, exa, etc and a significant number of
  Rust based libraries (more than 450). Buster ships with Rustc 1.32.
</para>

<para>
  Updates of other desktop applications include the upgrade to
  Evolution<indexterm><primary>Evolution</primary></indexterm> 3.22.
</para>

<!-- JFS:
Might it be useful point to http://distrowatch.com/table.php?distribution=debian ?
This provides a more comprehensive comparison among different releases -->

<para>
Among many others, this release also includes the following software updates:
</para>
<informaltable pgwide="1">
  <tgroup cols="3">
    <colspec align="justify"/>
    <colspec align="justify"/>
    <colspec align="justify"/>
    <!-- colspec align="justify" colwidth="3*"/ -->
    <thead>
      <row>
	<entry>Package</entry>
	<entry>Version in &oldrelease; (&oldreleasename;)</entry>
	<entry>Version in &release; (&releasename;)</entry>
      </row>
    </thead>
    <tbody>
      <row id="new-apache2">
	<entry>Apache<indexterm><primary>Apache</primary></indexterm></entry>
	<entry>2.4.25</entry>
	<entry>2.4.38</entry>
      </row>
      <row id="new-bind9">
	<entry>BIND<indexterm><primary>BIND</primary></indexterm> <acronym>DNS</acronym> Server</entry>
	<entry>9.10</entry>
	<entry>9.11</entry>
      </row>
<!--
      <row id="new-chromium">
	<entry>Chromium<indexterm><primary>Chromium</primary></indexterm></entry>
	<entry>53.0</entry>
	<entry>53.0</entry>
      </row>
      <row id="new-courier">
	<entry>Courier<indexterm><primary>Courier</primary></indexterm> <acronym>MTA</acronym></entry>
	<entry>0.73</entry>
	<entry>0.76</entry>
      </row>
-->
      <row id="new-cryptsetup">
        <entry>Cryptsetup<indexterm><primary>Cryptsetup</primary></indexterm></entry>
	<entry>1.7</entry>
	<entry>2.1</entry>
      </row>
<!--
      <row id="new-dia">
	<entry>Dia<indexterm><primary>Dia</primary></indexterm></entry>
	<entry>0.97.2</entry>
	<entry>0.97.3</entry>
        </row>
-->
      <row id="new-dovecot">
	<entry>Dovecot<indexterm><primary>Dovecot</primary></indexterm> <acronym>MTA</acronym></entry>
	<entry>2.2.27</entry>
	<entry>2.3.4</entry>
      </row>
      <row id="new-emacs">
	<entry>Emacs</entry>
	<entry>24.5 and 25.1</entry>
	<entry>26.1</entry>
      </row>
      <row id="new-exim4">
	<entry>Exim<indexterm><primary>Exim</primary></indexterm> default e-mail server</entry>
	<entry>4.89</entry>
	<entry>4.92</entry>
      </row>
<!--
      <row id="new-firefox">
	<entry>Firefox<indexterm><primary>Firefox</primary></indexterm></entry>
	<entry>45.5 (AKA Iceweasel)</entry>
	<entry>50.0</entry>
      </row>
-->
      <row id="new-gcc">
	<entry><acronym>GNU</acronym> Compiler Collection as default compiler<indexterm><primary>GCC</primary></indexterm></entry>
	<entry>6.3</entry>
	<entry>7.4 and 8.3</entry>
      </row>
      <row id="new-gimp">
	<entry><acronym>GIMP</acronym><indexterm><primary>GIMP</primary></indexterm></entry>
	<entry>2.8.18</entry>
	<entry>2.10.8</entry>
      </row>
      <row id="new-gnupg">
	<entry>GnuPG<indexterm><primary>GnuPG</primary></indexterm></entry>
	<entry>2.1</entry>
	<entry>2.2</entry>
      </row>
      <row id="new-inkscape">
	<entry>Inkscape<indexterm><primary>Inkscape</primary></indexterm></entry>
	<entry>0.92.1</entry>
	<entry>0.92.4</entry>
      </row>
      <row id="new-libc6">
	<entry>the <acronym>GNU</acronym> C library</entry>
	<entry>2.24</entry>
	<entry>2.28</entry>
      </row>
      <row id="new-lighttpd">
	<entry>lighttpd</entry>
	<entry>1.4.45</entry>
	<entry>1.4.53</entry>
      </row>
      <row id="new-linux-image">
        <entry>Linux kernel image</entry>
        <entry>4.9 series</entry>
        <entry>4.19 series</entry>
      </row>
      <row id="llvm-toolchain">
        <entry>LLVM/Clang toolchain</entry>
        <entry>3.7</entry>
        <entry>6.0.1 and 7.0.1 (default)</entry>
      </row>

    <row id="new-mariadb">
	<entry>MariaDB<indexterm><primary>MariaDB</primary></indexterm></entry>
	<entry>10.1</entry>
	<entry>10.3</entry>
      </row>

      <row id="new-nginx">
	<entry>Nginx<indexterm><primary>Nginx</primary></indexterm></entry>
	<entry>1.10</entry>
	<entry>1.14</entry>
      </row>
<!--
      <row id="new-openldap">
	<entry>OpenLDAP</entry>
	<entry>2.4.44</entry>
	<entry>2.4.47</entry>
      </row>
-->
      <row id="new-openjdk">
	<entry>OpenJDK<indexterm><primary>OpenJDK</primary></indexterm></entry>
	<entry>8</entry>
	<entry>8 and 11</entry>
      </row>
      <row id="new-openssh">
	<entry>OpenSSH<indexterm><primary>OpenSSH</primary></indexterm></entry>
	<entry>7.4p1</entry>
	<entry>7.9p1</entry>
      </row>
      <row id="new-perl">
	<entry>Perl<indexterm><primary>Perl</primary></indexterm></entry>
	<entry>5.24</entry>
	<entry>5.28</entry>
      </row>
      <row id="new-php">
	<entry><acronym>PHP</acronym><indexterm><primary>PHP</primary></indexterm></entry>
	<entry>7.0</entry>
	<entry>7.3</entry>
      </row>
      <row id="new-postfix">
	<entry>Postfix<indexterm><primary>Postfix</primary></indexterm> <acronym>MTA</acronym></entry>
	<entry>3.1.8</entry>
	<entry>3.3.2</entry>
      </row>
      <row id="new-postgresql">
	<entry>PostgreSQL<indexterm><primary>PostgreSQL</primary></indexterm></entry>
	<entry>9.6</entry>
	<entry>11</entry>
      </row>
<!--
      <row id="new-python">
	<entry>Python</entry>
	<entry>2.6</entry>
	<entry>2.7</entry>
      </row>
-->
      <row id="new-python3">
	<entry>Python 3</entry>
	<entry>3.5.3</entry>
	<entry>3.7.2</entry>
      </row>
      <row id="new-rustc">
	<entry>Rustc</entry>
	<entry></entry>
	<entry>1.32</entry>
      </row>
      <row id="new-samba">
	<entry>Samba</entry>
	<entry>4.5</entry>
	<entry>4.9</entry>
      </row>
      <row id="new-vim">
	<entry>Vim</entry>
	<entry>8.0</entry>
	<entry>8.1</entry>
      </row>
    </tbody>
  </tgroup>
</informaltable>

<programlisting condition="fixme">
 TODO: (JFS) List other server software? RADIUS? Streaming ?
</programlisting>

<section id="cd">
<title>CDs, DVDs, and BDs</title>
<para>
The official &debian; distribution now ships on 12 to 14 binary
<acronym>DVD</acronym>s <indexterm><primary>DVD</primary></indexterm>
(depending on the architecture) and 12 source
<acronym>DVD</acronym>s. Additionally, there is a
<emphasis>multi-arch</emphasis> <acronym>DVD</acronym>, with a subset
of the release for the <literal>amd64</literal> and
<literal>i386</literal> architectures, along with the source
code. &debian; is also released as Blu-ray
<indexterm><primary>Blu-ray</primary></indexterm>
(<acronym>BD</acronym>) and dual layer Blu-ray
<indexterm><primary>dual layer Blu-ray</primary></indexterm>
(<acronym>DLBD</acronym>) images for the <literal>amd64</literal> and
<literal>i386</literal> architectures, and also for source
code.
</para>
</section>

<section id="apparmor">
  <!-- stretch to buster -->
  <title>AppArmor enabled per default</title>
  <para>
    &debian; &releasename; has <literal>AppArmor</literal> enabled per
    default. <literal>AppArmor</literal> is a mandatory access control
    framework that allows to restrict programs' capabilities like read,
    write and execute permissions on files or mount, ptrace and signal
    permissions by defining per-program profiles.
  </para>
  <para>
    The default <systemitem role="package">apparmor</systemitem> package
    ships with AppArmor profiles for several programs. More profiles can
    be found in the <systemitem role="package">apparmor-profiles</systemitem>
    and <systemitem role="package">apparmor-profiles-extra</systemitem>
    packages. The latter also ships experimental profiles that need to
    be enabled manually.
  </para>
  <para>
    <literal>AppArmor</literal> is pulled in due to a
    <literal>Recommends</literal> by the &releasename; Linux kernel package.
    On systems that are configured to not install <literal>Recommends</literal>
    per default, the <systemitem role="package">apparmor</systemitem> package
    can be installed manually in order to enable <literal>AppArmor</literal>.
  </para>
</section>

<section id="de-manpages">
  <!-- stretch to buster -->
  <title>Substantial improved man pages for German speaking users</title>
  <para>
    The documentation (<literal>man</literal>-pages) for several projects like
    <systemitem role="package">systemd</systemitem>, <systemitem
    role="package">util-linux</systemitem> and <systemitem
    role="package">mutt</systemitem> have been substantially extended and
    added. Please install <systemitem role="package">manpages-de</systemitem>
    to benefit from the improvements. During the lifetime of &releasename;
    backports of further improvements / translations will be provided within
    the <literal>backports</literal> archive.
  </para>
</section>

<section id="nftables">
  <!-- stretch to buster -->
  <title>Network filtering based on nftables framework by default</title>
  <para>
    Starting with <systemitem role="package">iptables</systemitem> v1.8.2 the
    binary package includes <literal>iptables-nft</literal> and
    <literal>iptables-legacy</literal>, two variants of the
    <literal>iptables</literal> command line interface. The nftables-based
    variant is the default in buster and works with the
    <literal>nf_tables</literal> Linux kernel subsystem. The legacy one uses
    the <literal>x_tables</literal> Linux kernel subsystem. Users can use the
    update-alternatives system to select one variant or the other.
  </para>
  <para>
    This applies to all related tools and utilities:
    <itemizedlist>
      <listitem><para>iptables</para></listitem>
      <listitem><para>iptables-save</para></listitem>
      <listitem><para>iptables-restore</para></listitem>
      <listitem><para>ip6tables</para></listitem>
      <listitem><para>ip6tables-save</para></listitem>
      <listitem><para>ip6tables-restore</para></listitem>
      <listitem><para>arptables</para></listitem>
      <listitem><para>arptables-save</para></listitem>
      <listitem><para>arptables-restore</para></listitem>
      <listitem><para>ebtables</para></listitem>
      <listitem><para>ebtables-save</para></listitem>
      <listitem><para>ebtables-restore</para></listitem>
    </itemizedlist>
  </para>
  <para>
    All these gained the <literal>-nft</literal> and <literal>-legacy</literal>
    variants as well. The -nft option is for users that don't want -or can't-
    migrate to the native <literal>nftables</literal> command line
    interface. However users are really enouraged to switch to
    <literal>nftables</literal> interface rather than using the old
    <literal>iptables</literal> interface.
  </para>
  <para>
    <literal>nftables</literal> provides a full replacement for
    <literal>iptables</literal>, with much better performance, a refreshed
    syntax, better support for IPv4/IPv6 dual-stack firewalls, full atomic
    operations for dynamic ruleset updates, a Netlink API for third party
    applications, faster packet classification through enhanced generic set and
    map infrastructures, and <ulink url="https://wiki.nftables.org">many other
    improvements</ulink>.
  </para>
  <para>
    This movement is in line with what other major Linux distributions are
    doing, like RedHat, that now uses <literal>nftables</literal> as <ulink
    url="https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8-beta/html-single/8.0_beta_release_notes/index#networking_2">default
    firewalling tool</ulink>.
  </para>
  <para>
    Also, please note that all <literal>iptables</literal> binaries are now
    installed in <literal>/usr/sbin</literal> instead of
    <literal>/sbin</literal>. A compatibility symlink is in place, but will be
    dropped after the buster release cycle. Please, don't use hardcoded binary
    paths in scripts or update them manually for the new location.
  </para>
  <para>
    Extensive documentation is available in package's README and NEWS files and
    on the <ulink url="&url-wiki;nftables">Debian Wiki</ulink>.
  </para>
</section>

<section id="cryptsetup-luks2">
  <!-- stretch to buster -->
  <title>Cryptsetup defaults to on-disk LUKS2 format</title>
  <para>
    Debian buster ships with <systemitem role="package">cryptsetup</systemitem>
    which brings the new on-disk <literal>LUKS2</literal> format per default.
    New <literal>LUKS</literal> volumes will use the <literal>LUKS2</literal>
    format per default.
  </para>
  <para>
    Other than the previous <literal>LUKS1</literal> format,
    <literal>LUKS2</literal> provides redundancy of metadata, detection of
    metadata corruption and configurable <literal>PBKDF</literal> algorithms.
    Authenticated encryption is supported as well but still marked as
    experimental.
  </para>
  <para>
    Existing <literal>LUKS1</literal> volumes will not be updated
    automatically. They can be converted, but not all
    <literal>LUKS2</literal> features will be available due to header
    size incompatibilities. See the <systemitem
    role="package">cryptsetup</systemitem> manpage for more information.
  </para>
</section>

</section>
</chapter>
